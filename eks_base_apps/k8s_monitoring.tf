locals {
    grafana_token = jsondecode(data.aws_secretsmanager_secret_version.grafana.secret_string)["grafana_token"]
}

data "template_file" "monitoring" {
    template = file("${path.module}/monitoring_values.yaml")
}

resource "kubernetes_namespace" "alloy" {
    metadata {
        name = "alloy"
    }
}

resource "kubernetes_secret" "mimir" {
    metadata {
        name = "prom-creds"
        namespace = kubernetes_namespace.alloy.metadata.0.name
    }
    data = {
        host = "https://prometheus-prod-13-prod-us-east-0.grafana.net"
        username = "1171240"
        password = local.grafana_token
    }
}

resource "kubernetes_secret" "tempo" {
    metadata {
        name = "tempo-creds"
        namespace = kubernetes_namespace.alloy.metadata.0.name
    }
    data = {
        host = "https://tempo-prod-04-prod-us-east-0.grafana.net"
        username = "683799"
        password = local.grafana_token
    }
}

resource "kubernetes_secret" "loki" {
    metadata {
        name = "loki-creds"
        namespace = kubernetes_namespace.alloy.metadata.0.name
    }
    data = {
        host = "https://logs-prod-006.grafana.net"
        username = "684695"
        password = local.grafana_token
    }
}

resource "kubernetes_secret" "pyroscope" {
    metadata {
        name = "pyro-creds"
        namespace = kubernetes_namespace.alloy.metadata.0.name
    }
    data = {
        host     = "https://profiles-prod-001.grafana.net"
        username = "731662"
        password = local.grafana_token
    }
}


resource "helm_release" "alloy" {
    name       = "alloy"
    repository = "https://grafana.github.io/helm-charts"
    chart      = "k8s-monitoring"
    version    = "1.6.16"
    namespace  = kubernetes_namespace.alloy.metadata.0.name
    values = [data.template_file.monitoring.rendered]
    depends_on = [
        kubernetes_secret.loki,
        kubernetes_secret.mimir,
        kubernetes_secret.pyroscope,
        kubernetes_secret.tempo,
    ]
}

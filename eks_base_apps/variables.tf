variable "name" {
    type = string
    default = "devblog"
}

data "aws_secretsmanager_secret" "grafana" {
    name = "grafana_token"
}

data "aws_secretsmanager_secret_version" "grafana" {
  secret_id = data.aws_secretsmanager_secret.grafana.id
}

data "aws_secretsmanager_secret" "maxmind" {
    name = "maxmind_license"
}

data "aws_secretsmanager_secret_version" "maxmind" {
  secret_id = data.aws_secretsmanager_secret.maxmind.id
}

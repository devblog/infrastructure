data "template_file" "external_dns" {
    template = file("${path.module}/external_dns_values.yaml")
    vars = {
        service_account = local.service_account
    }
}

resource "kubernetes_namespace" "dns" {
    metadata {
        name = "dns"
    }
}

resource "aws_route53_zone" "this" {
  name = "paulbecotte.com"
}

locals {
    service_account = "dns"
}

resource "kubernetes_service_account" "dns" {
    metadata {
        name = local.service_account
        namespace = kubernetes_namespace.dns.metadata.0.name
        annotations = {
            "eks.amazonaws.com/role-arn" = module.dns_irsa_role.iam_role_arn
        }
    }
}

module "dns_irsa_role" {
  source    = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"

  role_name = "external_dns"
  attach_external_dns_policy = true

  external_dns_hosted_zone_arns         = [aws_route53_zone.this.arn]
  oidc_providers = {
    main = {
      provider_arn               = data.aws_iam_openid_connect_provider.cluster.arn
      namespace_service_accounts = ["${kubernetes_namespace.dns.metadata.0.name}:${local.service_account}"]
    }
  }
}

resource "helm_release" "external_dns" {
    name       = "external-dns"
    repository = "https://charts.bitnami.com/bitnami"
    chart      = "external-dns"
    version    = "6.20.3"
    namespace  = "dns"

    values = [data.template_file.external_dns.rendered]
    depends_on = [
      helm_release.alloy
    ]
}

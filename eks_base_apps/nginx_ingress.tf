locals {
    maxmind_license = jsondecode(data.aws_secretsmanager_secret_version.maxmind.secret_string)["maxmind_license"]
}

resource "kubernetes_namespace" "nginx" {
    metadata {
        name = "nginx"
    }
}

resource "aws_acm_certificate" "cert" {
  domain_name       = "paulbecotte.com"
  validation_method = "DNS"
  subject_alternative_names = ["*.paulbecotte.com"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.this.zone_id
}

data "template_file" "nginx_ingress" {
  template = file("${path.module}/nginx_ingress_values.yaml")
    vars = {
        cert_arn = aws_acm_certificate.cert.arn
    }
}

resource "helm_release" "nginx_ingress" {
    name       = "nginx"
    repository = "https://kubernetes.github.io/ingress-nginx"
    chart      = "ingress-nginx"
    version    = "4.11.3"
    namespace  = kubernetes_namespace.nginx.metadata.0.name
    set_sensitive {
      name  = "controller.maxmindLicenseKey"
      value = local.maxmind_license
    }
    values = [data.template_file.nginx_ingress.rendered]
    depends_on = [helm_release.alloy]
}

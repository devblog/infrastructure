resource "kubernetes_manifest" "nodepool" {
    manifest = {
        apiVersion = "karpenter.sh/v1"
        kind = "NodePool"
        metadata = {
            name = "eks-auto-mode"
        }
        spec = {
            disruption = {
                budgets = [
                    {
                        nodes = "10%"
                    }
                ]
                consolidateAfter = "30s"
                consolidationPolicy = "WhenEmptyOrUnderutilized"
            }
            template = {
                spec = {
                    nodeClassRef = {
                        group = "eks.amazonaws.com"
                        kind = "NodeClass"
                        name = "default"
                    }
                    terminationGracePeriod = "1h"
                    requirements = [
                        {
                            key = "karpenter.sh/capacity-type"
                            operator = "In"
                            values = ["on-demand", "spot"]
                        },
                        {
                            key = "kubernetes.io/arch"
                            operator = "In"
                            values = ["amd64"]
                        },
                        {
                            key = "eks.amazonaws.com/instance-family"
                            operator = "In"
                            values = ["t3"]
                        },
                    ]
                }

            }
        }
    }
    field_manager {
        force_conflicts = true
    }
}

terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "pbecotte-tf-state"
    key = "infrastructure.eks_cluster.tfstate"
  }
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.31.0"
    }
  }
}

################################################################################
# Cluster
################################################################################

resource "aws_eks_cluster" "this" {
  name                      = var.name
  role_arn                  = aws_iam_role.this.arn
  version                   = var.cluster_version
  enabled_cluster_log_types = []
  bootstrap_self_managed_addons = false

  access_config {
      authentication_mode = "API"
  }

  compute_config {
    enabled       = true
    node_pools    = ["system"]
    node_role_arn = aws_iam_role.node_v2.arn
  }

  kubernetes_network_config {
    elastic_load_balancing {
      enabled = true
    }
  }

  storage_config {
    block_storage {
      enabled = true
    }
  }

  vpc_config {
    security_group_ids      = [aws_security_group.cluster.id]
    subnet_ids              = module.vpc.public_subnets
    endpoint_private_access = true
    endpoint_public_access  = true
    public_access_cidrs     = ["0.0.0.0/0"]
  }

  depends_on = [
    aws_iam_role_policy_attachment.this,
  ]
}

# resource "aws_eks_access_entry" "node" {
#   cluster_name      = aws_eks_cluster.this.name
#   principal_arn     = aws_iam_role.node_v2.arn
#   kubernetes_groups = []
#   type              = "EC2"
# }
#
# resource "aws_eks_access_policy_association" "node" {
#     cluster_name = aws_eks_cluster.this.name
#     policy_arn = "arn:aws:eks::aws:cluster-access-policy/AmazonEKSAutoNodePolicy"
#     principal_arn = aws_iam_role.node_v2.arn
#     access_scope {
#         type = "cluster"
#     }
# }

resource "aws_eks_access_entry" "admin" {
  cluster_name      = aws_eks_cluster.this.name
  principal_arn     = data.aws_iam_session_context.current.issuer_arn
  kubernetes_groups = []
  type              = "STANDARD"
}

resource "aws_eks_access_policy_association" "admin" {
    cluster_name = aws_eks_cluster.this.name
    policy_arn = "arn:aws:eks::aws:cluster-access-policy/AmazonEKSClusterAdminPolicy"
    principal_arn = data.aws_iam_session_context.current.issuer_arn
    access_scope {
        type = "cluster"
    }
}

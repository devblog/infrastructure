resource "aws_security_group" "cluster" {
  name_prefix = "${var.name}-"
  description = "EKS security group"
  vpc_id      = module.vpc.vpc_id
  tags = {
    "karpenter.sh/discovery" = var.name
  }
  lifecycle {
    create_before_destroy = true
  }
}

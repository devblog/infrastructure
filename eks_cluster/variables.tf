variable "name" {
    type = string
    default = "devblog"
}

variable "vpc_cidr" {
    type = string
    default = "10.0.0.0/16"
}

variable "cluster_version" {
    type = string
    default = "1.31"
}

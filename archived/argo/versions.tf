terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
    helm = {
      source = "hashicorp/helm"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    local = {
      source = "hashicorp/local"
    }
    tls = {
      source = "hashicorp/tls"
    }
  }
  required_version = ">= 0.13"
}

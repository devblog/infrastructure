variable "GITLAB_TOKEN" {}

provider "gitlab" {
    token = var.GITLAB_TOKEN
}

resource "tls_private_key" "argocd" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "gitlab_deploy_key" "sample_deploy_key" {
    project = "14320273"
    title = "argo deploy key"
    key = tls_private_key.argocd.public_key_openssh
}

resource "gitlab_deploy_token" "devlog" {
  group = "devblog"
  name = "Devblog deploy token"
  username = "devblog-token"
  expires_at = "2024-03-14T00:00:00Z"
  scopes = [
    "read_repository",
    "read_registry",
  ]
}

resource "gitlab_project_hook" "devblogreact" {
  project = "devblog/devblogreact"
  url = "https://argo.paulbecotte.com/api/webhook"
  tag_push_events = true
}

resource "gitlab_project_hook" "devblogflask" {
  project = "devblog/devblogflask"
  url = "https://argo.paulbecotte.com/api/webhook"
  tag_push_events = true
}

resource "gitlab_project_hook" "devblog" {
  project = "devblog/devblog"
  url = "https://argo.paulbecotte.com/api/webhook"
  tag_push_events = true
}

resource "gitlab_project_variable" "aws_access_key" {
  project = "devblog/devblog"
  masked = false
  protected = true
  key = "TF_VAR_REAL_AWS_ACCESS_KEY_ID"
  value = var.REAL_AWS_ACCESS_KEY_ID
}

resource "gitlab_project_variable" "aws_access_key_secret" {
  project = "devblog/devblog"
  masked = true
  protected = true
  key = "TF_VAR_REAL_AWS_SECRET_ACCESS_KEY"
  value = var.REAL_AWS_SECRET_ACCESS_KEY
}

resource "gitlab_project_variable" "fake_aws_access_key" {
  project = "devblog/devblog"
  masked = false
  protected = true
  key = "AWS_ACCESS_KEY_ID"
  value = var.AWS_ACCESS_KEY_ID
}

resource "gitlab_project_variable" "fake_aws_access_key_secret" {
  project = "devblog/devblog"
  masked = true
  protected = true
  key = "AWS_SECRET_ACCESS_KEY"
  value = var.AWS_SECRET_ACCESS_KEY
}

resource "gitlab_project_variable" "do_token" {
  project = "devblog/devblog"
  masked = true
  protected = true
  key = "TF_VAR_DO_TOKEN"
  value = var.DO_TOKEN
}

resource "gitlab_project_hook" "infra" {
  project = "devblog/infrastructure"
  url = "https://argo.paulbecotte.com/api/webhook"
  tag_push_events = true
}

resource "kubernetes_secret" "gitlab-key" {
  metadata {
    name = "gitlab-key"
    namespace = kubernetes_namespace.argo.metadata[0].name
  }
  data = {
    private-key: tls_private_key.argocd.private_key_pem
    devblog-token: gitlab_deploy_token.devlog.token
    devblog-username: "devblog-token"
  }
}

variable "DO_TOKEN" {}
variable "AWS_ACCESS_KEY_ID" {}
variable "AWS_SECRET_ACCESS_KEY" {}
variable "REAL_AWS_ACCESS_KEY_ID" {}
variable "REAL_AWS_SECRET_ACCESS_KEY" {}


provider "digitalocean" {
  token = var.DO_TOKEN
}

# TF State
terraform {
  backend "s3" {
    skip_credentials_validation = true
    skip_metadata_api_check = true
    endpoint = "sfo2.digitaloceanspaces.com"
    region = "us-east-1"
    bucket = "devblog-devops"
    key = "infrastructure.argo.tfstate"
  }
}

data "digitalocean_kubernetes_cluster" "pbecotte" {
  name = "pbecotte"
}

provider "kubernetes" {
  host  = data.digitalocean_kubernetes_cluster.pbecotte.endpoint
  token = data.digitalocean_kubernetes_cluster.pbecotte.kube_config[0].token
  cluster_ca_certificate = base64decode(
    data.digitalocean_kubernetes_cluster.pbecotte.kube_config[0].cluster_ca_certificate
  )
}

provider "helm" {
    kubernetes {
        host  = data.digitalocean_kubernetes_cluster.pbecotte.endpoint
        token = data.digitalocean_kubernetes_cluster.pbecotte.kube_config[0].token
        cluster_ca_certificate = base64decode(
            data.digitalocean_kubernetes_cluster.pbecotte.kube_config[0].cluster_ca_certificate
        )
    }
}

data "local_file" "argo-values" {
  filename = "${path.module}/argo_values.yaml"
}

resource "kubernetes_namespace" "argo" {
    metadata {
        name = "argo"
    }
}

resource "helm_release" "argo" {
  name       = "argocd"
  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argo-cd"
  version    = "4.8.2"
  namespace  = kubernetes_namespace.argo.metadata[0].name

  values = [
    data.local_file.argo-values.content
  ]
}

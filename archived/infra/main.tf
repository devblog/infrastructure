variable "DO_TOKEN" {}
variable "GITLAB_RUNNER_TOKEN" {}
variable "AWS_ACCESS_KEY_ID" {}
variable "AWS_SECRET_ACCESS_KEY" {}

provider "digitalocean" {
    token = var.DO_TOKEN
    spaces_access_id = var.AWS_ACCESS_KEY_ID
    spaces_secret_key = var.AWS_SECRET_ACCESS_KEY
}

# TF State
terraform {
  backend "s3" {
    skip_credentials_validation = true
    skip_metadata_api_check = true
    endpoint = "sfo2.digitaloceanspaces.com"
    region = "us-east-1"
    bucket = "devblog-devops"
    key = "infrastructure.infra.tfstate"
  }
}

data "digitalocean_kubernetes_cluster" "pbecotte" {
  name = "pbecotte"
}

provider "kubernetes" {
  host  = data.digitalocean_kubernetes_cluster.pbecotte.endpoint
  token = data.digitalocean_kubernetes_cluster.pbecotte.kube_config[0].token
  cluster_ca_certificate = base64decode(
    data.digitalocean_kubernetes_cluster.pbecotte.kube_config[0].cluster_ca_certificate
  )
  experiments {
        manifest_resource = true
    }
}

resource "kubernetes_namespace" "external-dns" {
    metadata {
        name = "external-dns"
    }
}

resource "kubernetes_namespace" "loki" {
    metadata {
        name = "loki"
    }
}

resource "kubernetes_namespace" "grafana" {
    metadata {
        name = "grafana"
    }
}

resource "kubernetes_namespace" "node-exporter" {
    metadata {
        name = "node-exporter"
    }
}

resource "kubernetes_namespace" "nginx-ingress" {
    metadata {
        name = "nginx-ingress"
    }
}

resource "kubernetes_namespace" "cert-manager" {
    metadata {
        name = "cert-manager"
    }
}

resource "kubernetes_namespace" "monitoring" {
    metadata {
        name = "monitoring"
    }
}

resource "kubernetes_namespace" "datadog" {
  metadata {
      name = "datadog"
  }
}

resource "kubernetes_secret" "digitaloceanapitoken" {
    metadata {
        name = "digitaloceanapitoken"
        namespace = kubernetes_namespace.external-dns.metadata[0].name
    }
    data = {
        digitalocean_api_token = var.DO_TOKEN
    }
}

resource "kubernetes_namespace" "p2g" {
  metadata {
    name = "peloton-to-garmin"
  }
}

data "local_file" "infra-app" {
    filename = "${path.module}/infra-app.yaml"
}

resource "kubernetes_manifest" "infra-app" {
  manifest = yamldecode(data.local_file.infra-app.content)
}

resource "kubernetes_namespace" "gitlab_runner" {
    metadata {
        name = "gitlab-runner"
    }
}

resource "digitalocean_spaces_bucket" "gitlab" {
    name = "devblog-gitlab-cache"
    region = "nyc3"
}

resource "kubernetes_secret" "gitlab_runner" {
    metadata {
        name = "gitlab-runner-secret"
        namespace = kubernetes_namespace.gitlab_runner.metadata.0.name
    }
    data = {
        "runner-registration-token" = var.GITLAB_RUNNER_TOKEN
        "runner-token": ""
        "accesskey": var.AWS_ACCESS_KEY_ID
        "secretkey": var.AWS_SECRET_ACCESS_KEY
    }
}

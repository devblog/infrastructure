variable "DO_TOKEN" {}
variable "AWS_ACCESS_KEY_ID" {}
variable "AWS_SECRET_ACCESS_KEY" {}

provider "digitalocean" {
  token = var.DO_TOKEN
}

# TF State
terraform {
  backend "s3" {
    skip_credentials_validation = true
    skip_metadata_api_check = true
    endpoint = "sfo2.digitaloceanspaces.com"
    region = "us-east-1"
    bucket = "devblog-devops"
    key = "infrastructure.devblog.tfstate"
  }
}

data "digitalocean_kubernetes_cluster" "pbecotte" {
  name = "pbecotte"
}

provider "kubernetes" {
  host  = data.digitalocean_kubernetes_cluster.pbecotte.endpoint
  token = data.digitalocean_kubernetes_cluster.pbecotte.kube_config[0].token
  cluster_ca_certificate = base64decode(
    data.digitalocean_kubernetes_cluster.pbecotte.kube_config[0].cluster_ca_certificate
  )
  experiments {
        manifest_resource = true
    }
}

resource "kubernetes_namespace" "devblog-release" {
  metadata {
    name = "devblog-release"
  }
}

resource "random_string" "postgres-password" {
  length = 16
  special = false
}

resource "kubernetes_secret" "postgres-password" {
  metadata {
    name = "postgres-password"
    namespace = kubernetes_namespace.devblog-release.metadata[0].name
  }
  data = {
    "postgres_password.txt" = random_string.postgres-password.result
  }
}

resource "kubernetes_secret" "aws-keys" {
  metadata {
    name = "aws-keys"
    namespace = kubernetes_namespace.devblog-release.metadata[0].name
  }
  data = {
    AWS_ACCESS_KEY_ID = var.AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY = var.AWS_SECRET_ACCESS_KEY
  }
}

resource "tls_private_key" "jwt" {
  algorithm   = "RSA"
  rsa_bits = 2048
}

resource "kubernetes_secret" "jwt-keys" {
  metadata {
    name = "jwt-key"
    namespace = kubernetes_namespace.devblog-release.metadata[0].name
  }
  data = {
    "private_key.pem" = tls_private_key.jwt.private_key_pem
    "public_key.pem" = tls_private_key.jwt.public_key_pem
  }
}

data "local_file" "argo-devblog-flask-manifest" {
  filename = "${path.module}/devblog-flask-app.yaml"
}

resource "kubernetes_manifest" "argo-devblog-flask-app" {
  manifest = yamldecode(data.local_file.argo-devblog-flask-manifest.content)
}

data "local_file" "argo-devblog-react-manifest" {
  filename = "${path.module}/devblog-react-app.yaml"
}

resource "kubernetes_manifest" "argo-devblog-react-app" {
  manifest = yamldecode(data.local_file.argo-devblog-react-manifest.content)
}

variable "DO_TOKEN" {}

provider "digitalocean" {
  token = var.DO_TOKEN
}

# TF State
terraform {
  backend "s3" {
    skip_credentials_validation = true
    skip_metadata_api_check = true
    endpoint = "sfo2.digitaloceanspaces.com"
    region = "us-east-1"
    bucket = "devblog-devops"
    key = "infrastructure.do.tfstate"
  }
}

data "digitalocean_kubernetes_versions" "basic" {
  version_prefix = "1.24."
}

resource "digitalocean_kubernetes_cluster" "paulbecotte" {
  name    = "pbecotte"
  region  = "nyc1"
  # Grab the latest version slug from `doctl kubernetes options versions`
  version = data.digitalocean_kubernetes_versions.basic.latest_version
  auto_upgrade = true
  surge_upgrade = true
  node_pool {
    name       = "base-pool"
    size       = "s-1vcpu-2gb"
    node_count = 1
  }
}

resource "digitalocean_kubernetes_node_pool" "default" {
    cluster_id = digitalocean_kubernetes_cluster.paulbecotte.id
    name       = "default"
    size       =  "s-2vcpu-4gb"
    min_nodes  = 1
    max_nodes  = 5
    auto_scale = true
    labels     = {
      type = "default"
    }
}

resource "digitalocean_kubernetes_node_pool" "gitlab" {
    cluster_id = digitalocean_kubernetes_cluster.paulbecotte.id
    name       = "gitlab"
    size       = "s-4vcpu-8gb"
    #size       =  "g-32vcpu-128gb"
    # DO doesn't support scale to 0
    min_nodes  = 1
    max_nodes  = 1
    auto_scale = true
    labels     = {
      type = "gitlab"
    }
    taint {
        key = "gitlab"
        value = "executor"
        effect = "NoSchedule"
    }
}

# data "template_file" "karpenter" {
#     template = file("${path.module}/karpenter_values.yaml")
#     vars = {
#         service_account = local.karpenter_service_account
#         queue = aws_sqs_queue.karpenter.name
#         karpenter_role_arn = module.karpenter_irsa_role.iam_role_arn
#         iam_instance_profile = aws_iam_instance_profile.node.name
#     }
# }
#
# resource "kubernetes_namespace" "karpenter" {
#     metadata {
#         name = "karpenter"
#     }
# }
#
# locals {
#     karpenter_service_account = "karpenter"
# }
#
# resource "aws_sqs_queue" "karpenter" {
#     name = "karpenter-devblog"
#     message_retention_seconds = 300
# }
#
# data "aws_iam_policy_document" "karpenter_sqs" {
#   statement {
#     sid    = "EC2InterruptionPolicy"
#     effect = "Allow"
#
#     principals {
#       type        = "Service"
#       identifiers = ["events.amazonaws.com", "sqs.amazonaws.com"]
#     }
#
#     actions   = ["sqs:SendMessage"]
#     resources = [aws_sqs_queue.karpenter.arn]
#   }
# }
#
# resource "aws_sqs_queue_policy" "karpenter" {
#   queue_url = aws_sqs_queue.karpenter.id
#   policy    = data.aws_iam_policy_document.karpenter_sqs.json
# }
#
# resource "aws_cloudwatch_event_rule" "scheduled_change_rule" {
#     event_pattern = jsonencode({
#         source = ["aws.health"]
#         detail-type = ["AWS Health Event"]
#     })
# }
#
# resource "aws_cloudwatch_event_target" "scheduled_change_rule" {
#     target_id = "karpenter_scheduled_change_rule"
#     rule = aws_cloudwatch_event_rule.scheduled_change_rule.name
#     arn = aws_sqs_queue.karpenter.arn
# }
#
# resource "aws_cloudwatch_event_rule" "spot_interrupt_rule" {
#     event_pattern = jsonencode({
#         source = ["aws.ec2"]
#         detail-type = ["EC2 Spot Instance Interruption Warning"]
#     })
# }
#
# resource "aws_cloudwatch_event_target" "spot_interrupt_rule" {
#     target_id = "karpenter_spot_interrupt_rule"
#     rule = aws_cloudwatch_event_rule.spot_interrupt_rule.name
#     arn = aws_sqs_queue.karpenter.arn
# }
#
# resource "aws_cloudwatch_event_rule" "rebalance_rule" {
#     event_pattern = jsonencode({
#         source = ["aws.ec2"]
#         detail-type = ["EC2 Instance Rebalance Recommendation"]
#     })
# }
#
# resource "aws_cloudwatch_event_target" "rebalance_rule" {
#     target_id = "karpenter_rebalance_rule"
#     rule = aws_cloudwatch_event_rule.rebalance_rule.name
#     arn = aws_sqs_queue.karpenter.arn
# }
#
# resource "aws_cloudwatch_event_rule" "instance_state_change_rule" {
#     event_pattern = jsonencode({
#         source = ["aws.ec2"]
#         detail-type = ["EC2 Instance State-change Notification"]
#     })
# }
#
# resource "aws_cloudwatch_event_target" "instance_state_change_rule" {
#     target_id = "karpenter_instance_state_change_rule"
#     rule = aws_cloudwatch_event_rule.instance_state_change_rule.name
#     arn = aws_sqs_queue.karpenter.arn
# }
#
# module "karpenter_irsa_role" {
#   source    = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
#
#   role_name = "karpenter"
#   attach_karpenter_controller_policy = true
#   karpenter_sqs_queue_arn = aws_sqs_queue.karpenter.arn
#   karpenter_controller_cluster_name = "devblog"
#
#   oidc_providers = {
#     main = {
#       provider_arn               = data.aws_iam_openid_connect_provider.cluster.arn
#       namespace_service_accounts = ["${kubernetes_namespace.karpenter.metadata.0.name}:${local.karpenter_service_account}"]
#     }
#   }
# }
#
# resource "aws_iam_instance_profile" "node" {
#     name = "karpenter_node"
#     path = "/"
#     role = "${var.name}-node"
# }
#
# resource "helm_release" "karpenter" {
#     name       = "karpenter"
#     repository = "oci://public.ecr.aws/karpenter"
#     chart      = "karpenter"
#     version    = "v0.27.5"
#     namespace  = kubernetes_namespace.karpenter.metadata.0.name
#     depends_on = []
#
#     values = [data.template_file.karpenter.rendered]
# }

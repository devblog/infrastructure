data "local_file" "prom_stack" {
  filename = "${path.module}/prom_stack_values.yaml"
}

data "template_file" "grafana_agent" {
    template = file("${path.module}/grafana_agent_values.yaml")
}

resource "kubernetes_namespace" "prometheus" {
    metadata {
        name = "prometheus"
    }
}

resource "kubernetes_namespace" "grafana" {
    metadata {
        name = "grafana"
    }
}

resource "random_password" "grafana" {
  length           = 12
  special          = false
}

resource "kubernetes_secret" "grafana" {
    metadata {
        name = "grafana-secret"
        namespace = kubernetes_namespace.prometheus.metadata.0.name
    }
    data = {
        admin-user = "admin"
        admin-password = random_password.grafana.result
    }
}

#resource "helm_release" "prom_stack" {
#    name       = "prom-stack"
#    repository = "https://prometheus-community.github.io/helm-charts"
#    chart      = "kube-prometheus-stack"
#    version    = "46.6.0"
#    namespace  = "prometheus"
#    values = [data.local_file.prom_stack.content]
#}

resource "helm_release" "grafana_agent" {
    name       = "grafana-agent"
    repository = "https://grafana.github.io/helm-charts"
    chart      = "k8s-monitoring"
    version    = "0.1.15"
    namespace  = kubernetes_namespace.grafana.metadata.0.name
    set_sensitive {
        name  = "externalServices.prometheus.basicAuth.password"
        value = var.grafana_token
    }
    set_sensitive {
        name  = "externalServices.loki.basicAuth.password"
        value = var.grafana_token
    }
    values = [data.template_file.grafana_agent.rendered]
}

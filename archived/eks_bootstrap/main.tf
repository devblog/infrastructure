terraform {
    backend "s3" {
        region = "us-east-1"
        bucket = "pbecotte-tf-state"
        key = "infrastructure.eks_bootstrap.tfstate"
    }
}

data "aws_eks_cluster" "this" {
    name = var.name
}

data "aws_eks_cluster_auth" "this" {
    name = var.name
}

data "aws_iam_openid_connect_provider" "cluster" {
    url = data.aws_eks_cluster.this.identity.0.oidc.0.issuer
}

provider "kubernetes" {
    host                   = data.aws_eks_cluster.this.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.this.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.this.token
}

provider "helm" {
    kubernetes {
        host                   = data.aws_eks_cluster.this.endpoint
        cluster_ca_certificate = base64decode(data.aws_eks_cluster.this.certificate_authority[0].data)
        token                  = data.aws_eks_cluster_auth.this.token
    }
}

variable "name" {
    type = string
    default = "devblog"
}

variable "grafana_token" {}
